#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include "meshQuad2D.h"


void meshParallelPrintQuad2D(mesh2D *mesh){

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    printf("rank %d: Nelements=" iintFormat " Nnodes=" iintFormat "\n",
         rank, mesh->Nelements, mesh->Nnodes);    

    iint *otherNelements = (iint*) calloc(size, sizeof(iint));
    MPI_Allgather(&(mesh->Nelements), 1, MPI_IINT,
                  otherNelements, 1, MPI_IINT,
                  MPI_COMM_WORLD);

    // allocate space
    iint *elementStarts = (iint*) calloc(size, sizeof(iint));

    iint r;
    for(r=1;r<size;++r){
       elementStarts[r] = elementStarts[r-1]+otherNelements[r-1];
    }


    int r1;
    iint e1, f1;
    for(r1=0;r1<size;++r1){

       MPI_Barrier(MPI_COMM_WORLD);
       if(rank==r1){
          fflush(stdout);
          if(r1==0)
            printf("EToE:\n");
          for(e1=0;e1<mesh->Nelements;++e1){
            iint id = e1*mesh->Nfaces;
            for(f1=0;f1<mesh->Nfaces;++f1){
                iint e2 = mesh->EToE[id+f1];
                iint f2 = mesh->EToF[id+f1];
                iint r2 = mesh->EToP[id+f1];
                if(e2==-1 || f2==-1)if(e2==-1 || f2==-1)
                   printf("(" iintFormat " " iintFormat ")=>X (" iintFormat "," iintFormat ")\n",
                        e1+elementStarts[r1], f1, e2, f2);
                else{
                   if(r2!=-1)
                      e2 += elementStarts[r2];
                   else
                      e2 += elementStarts[r1];

                    printf("(" iintFormat " " iintFormat ")=>(" iintFormat " " iintFormat ")\n",
                         e1+elementStarts[r1], f1, e2, f2);

                }
            }
            fflush(stdout);
          }    

       }
       MPI_Barrier(MPI_COMM_WORLD);
    }

    free(otherNelements);
    free(elementStarts);

}
