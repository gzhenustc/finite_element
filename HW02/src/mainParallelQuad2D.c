#include "meshQuad2D.h"
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv){

  /* start MPI */
  MPI_Init(&argc, &argv);

  mesh2D *mesh;

  mesh = meshParallelReaderQuad2D(argv[1]);
  
  meshParallelConnectQuad2D(mesh);
  
  meshParallelPrintQuad2D(mesh);

  //create vtu file name
  char vtuName[BUFSIZ];
  sprintf(vtuName, "%s.vtu", strtok(argv[1], "."));
  meshVTU2D(mesh, vtuName);

  //create etov file name
  char etovName[BUFSIZ];
  sprintf(etovName,"%s.etov", strtok(argv[1], "."));
  meshPrintFile(mesh, etovName);

  /* finalize MPI */
  MPI_Finalize();
  
  return 0;
}
