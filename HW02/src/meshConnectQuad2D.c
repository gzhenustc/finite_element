#include<stdio.h>
#include<stdlib.h>
#include "meshQuad2D.h"

typedef struct{
   
   iint element;
   iint face;
   iint elementNeighbor;  // neighbor element
   iint faceNeighbor;  // neighbor face
   iint v1;
   iint v2;  //vertices for each line

}face_t;

int compareVertices(const void *a,
                    const void *b){
    face_t *fa = (face_t*) a;
    face_t *fb = (face_t*) b;

    if(fa->v1 < fb->v1) return -1;
    if(fa->v1 > fb->v1) return +1;

    if(fa->v2 < fb->v2) return -1;
    if(fa->v2 > fb->v2) return +1;

    return 0;
}

int compareFaces(const void *a,
                 const void *b){
    face_t *fa = (face_t*) a;
    face_t *fb = (face_t*) b;

    if(fa->element < fb->element) return -1;
    if(fa->element > fb->element) return +1;

    if(fa->face < fb->face) return -1;
    if(fa->face > fb->face) return +1;

    return 0;
}

void meshConnectQuad2D(mesh2D *mesh){
   iint e;
   iint f;

   face_t *faces = 
     (face_t*) calloc(mesh->Nelements*mesh->Nfaces,
                      sizeof(face_t));
   
   int cnt=0; //counting varible
   for(e=0;e<mesh->Nelements;++e){
     for(f=0;f<mesh->Nfaces;++f){
        
        int v1 = mesh->EToV[e*mesh->Nverts + f];
        int v2 = mesh->EToV[e*mesh->Nverts + (f+1)%mesh->Nverts];

        faces[cnt].v1 = mymax(v1,v2);
        faces[cnt].v2 = mymin(v1,v2);
 
        faces[cnt].element = e;
        faces[cnt].face = f;

        faces[cnt].elementNeighbor = -1;
        faces[cnt].faceNeighbor = -1;

        ++cnt;
        
     }       
   }
   
   //sort the vertices
   qsort(faces,
         mesh->Nelements*mesh->Nfaces,
         sizeof(face_t),
         compareVertices);
  
   for(cnt=0;cnt<mesh->Nelements*mesh->Nfaces-1;++cnt){
        if(!compareVertices(faces+cnt, faces+cnt+1)){
          //find a match, store the element and face
          faces[cnt].elementNeighbor = faces[cnt+1].element;
          faces[cnt].faceNeighbor = faces[cnt+1].face;
          
          faces[cnt+1].elementNeighbor = faces[cnt].element;
          faces[cnt+1].faceNeighbor = faces[cnt].face;
        }
   }
   
   //sort the faces
   qsort(faces,
         mesh->Nelements*mesh->Nfaces,
         sizeof(face_t),
         compareFaces);
    
   //element to element connectivity; element to face conncetivity
   mesh->EToE = (iint*) calloc(mesh->Nelements*mesh->Nfaces,
                               sizeof(iint));
   mesh->EToF = (iint*) calloc(mesh->Nelements*mesh->Nfaces,
                               sizeof(iint)); 

   cnt=0;
   for(e=0;e<mesh->Nelements;++e){
     for(f=0;f<mesh->Nfaces;++f){
         mesh->EToE[cnt] = faces[cnt].elementNeighbor;
         mesh->EToF[cnt] = faces[cnt].faceNeighbor;

         ++cnt;   
        
     }
   }


}
