#!/usr/bin/env python
import math
import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
from scipy import linalg
from matplotlib.ticker import MaxNLocator
import matplotlib.cm as cm


f=open('sampleMeshQuad.etov','r')
lines=f.readlines()
f.close()

#number of cells
numcell = int(lines[0].split()[1])
numvert = 4

#rank start line and number of cell per rank
rankstart=[]
rankcell=np.zeros((numcell,),dtype=np.int)

for i in range(0,len(lines)):
    if(lines[i].startswith("rank")):
         rankstart.append(i)

ranksize = len(rankstart)

for i in range(0,ranksize-1):
    temrank = int(lines[rankstart[i]].split()[1])
    i_s=rankstart[i]+1
    i_e=rankstart[i+1]
    for j in range(i_s,i_e):
         cnt=int(lines[j].split()[0])
         rankcell[cnt]=temrank
        
#last rank
temrank = int(lines[rankstart[ranksize-1]].split()[1])
i_s=rankstart[ranksize-1]+1
i_e=len(lines)
for j in range(i_s,i_e):
     cnt=int(lines[j].split()[0])
     rankcell[cnt]=temrank


print rankstart
print rankcell
        
#plot
fig,ax=plt.subplots()
tick=int(math.sqrt(numcell))     
print tick
ax.xaxis.set_major_locator(MaxNLocator(tick))
ax.yaxis.set_major_locator(MaxNLocator(tick))
plt.grid(color='k',linestyle='-',linewidth=1)
vel3=np.zeros((tick,tick),dtype=np.int)
for i in range(tick):
   for j in range(tick):
      index=i*tick+j
      vel3[i][j]=rankcell[index]
im=ax.imshow(vel3,origin='upper',extent=(0,1,1,0),cmap=cm.jet,vmin=0, vmax=ranksize-1,interpolation='none')
cbar = plt.colorbar(im, ticks=np.arange(0,ranksize,1,dtype=np.int))
fig.savefig('cell.png')
#plt.show()

