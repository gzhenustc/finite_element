#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mpi.h"
#include "meshQuad2D.h"

/* 
   purpose: read gmsh quadrilateral meshes 
*/
mesh2D* meshParallelReaderQuad2D(char *fileName){

  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  mesh2D *mesh = (mesh2D*) calloc(1, sizeof(mesh2D));
  mesh->Nverts = 4; // number of vertices per element
  mesh->Nfaces = 4; // number of faces per element
  

  if(rank == 0){
    FILE *fp = fopen(fileName, "r");
    int n;

    if(fp==NULL){
      printf("meshParallelReaderQuad2D: could not load file %s\n", fileName);
      exit(0);
    }

    char buf[BUFSIZ];
    do{
      fgets(buf, BUFSIZ, fp);
    }while(!strstr(buf, "$Nodes"));

    /* read number of nodes in mesh */
    fgets(buf, BUFSIZ, fp);
    sscanf(buf, "%d", &(mesh->Nnodes));

    /* allocate space for node coordinates */
    mesh->VX = (dfloat*) calloc(mesh->Nnodes, sizeof(dfloat));
    mesh->VY = (dfloat*) calloc(mesh->Nnodes, sizeof(dfloat));

    /* load nodes */
    for(n=0;n<mesh->Nnodes;++n){
      fgets(buf, BUFSIZ, fp);
      sscanf(buf, "%*d%f%f", mesh->VX+n, mesh->VY+n);
    }
    
    /* look for section with Element node data */
    do{
      fgets(buf, BUFSIZ, fp);
    }while(!strstr(buf, "$Elements"));

    /* read number of nodes in mesh */
    fgets(buf, BUFSIZ, fp);
    sscanf(buf, "%d", &(mesh->Nelements));

    /* find number of quadrilaterals */
    fpos_t fpos;
    fgetpos(fp, &fpos);

    int Nquads = 0;
    for(n=0;n<mesh->Nelements;++n){
      iint elementType;
      fgets(buf, BUFSIZ, fp);
      sscanf(buf, "%*d%d", &elementType);
      if(elementType==3) ++Nquads;
    }

    // rewind to start of elements
    fsetpos(fp, &fpos);

    /* allocate space for Element node index data */
    iint *tmpEToV
       = (iint*) calloc(Nquads*mesh->Nverts,
              sizeof(iint));

    /* scan through file looking for quadrilateral elements */
    iint cnt = 0;
    for(n=0;n<mesh->Nelements;++n){
      iint elementType, v1, v2, v3, v4;
      fgets(buf, BUFSIZ, fp);
      sscanf(buf, "%*d%d", &elementType);
      if(elementType==3){  // quadrilateral
           sscanf(buf, "%*d%*d%*d%*d%*d %d%d%d%d", 
  	        &v1, &v2, &v3, &v4);
           tmpEToV[cnt*mesh->Nverts+0] = v1-1;
           tmpEToV[cnt*mesh->Nverts+1] = v2-1;
           tmpEToV[cnt*mesh->Nverts+2] = v3-1;
           tmpEToV[cnt*mesh->Nverts+3] = v4-1;
           ++cnt;
      }
    }
    fclose(fp);

    //mesh->Nelements = Nquads;
    
    int chunk = Nquads/size;
    int remainder = Nquads - chunk*size;

    //send message out
    int r; //for each rank
    for(r=1;r<size;++r){
        //calculate the number of elements for each process
        int NquadsLocal = chunk + (r<remainder);
        
        /* where do these elements start ? */
        int start = r*chunk + mymin(r, remainder);
        int end = start + NquadsLocal-1;

        //request messages to be sent
        MPI_Request requestOut, requestOut_1, requestOut_2, requestOut_3;
        MPI_Status statusOut, statusOut_1, statusOut_2, statusOut_3;        
        int tag = 9, tag_1 = 99, tag_2 = 999, tag_3 = 9999;
        int dest = r;
    
        //send Nnodes, NquadsLocal
        iint *messageOut = (iint*) calloc(2, sizeof(iint));
        messageOut[0] = mesh->Nnodes;
        messageOut[1] = NquadsLocal;
        
        MPI_Isend(messageOut, 2, MPI_INT, dest, tag, MPI_COMM_WORLD, &requestOut);
        MPI_Wait(&requestOut, &statusOut);

        //send EToV to each process
        iint *messageOut_1 = (iint*) calloc(NquadsLocal*mesh->Nverts,
                                            sizeof(iint));
        int i, cnt;
        for(i=0;i<NquadsLocal;i++){
            cnt = start + i;
            messageOut_1[i*mesh->Nverts+0] = tmpEToV[cnt*mesh->Nverts+0];
            messageOut_1[i*mesh->Nverts+1] = tmpEToV[cnt*mesh->Nverts+1];
            messageOut_1[i*mesh->Nverts+2] = tmpEToV[cnt*mesh->Nverts+2];
            messageOut_1[i*mesh->Nverts+3] = tmpEToV[cnt*mesh->Nverts+3];
        }
        
        MPI_Isend(messageOut_1, NquadsLocal*mesh->Nverts, MPI_INT, 
                  dest, tag_1, MPI_COMM_WORLD, &requestOut_1);
        MPI_Wait(&requestOut_1, &statusOut_1);
        
        //send out VX and VY
        MPI_Isend(mesh->VX, mesh->Nnodes, MPI_FLOAT,
                  dest, tag_2, MPI_COMM_WORLD, &requestOut_2);
        MPI_Wait(&requestOut_2, &statusOut_2);

        MPI_Isend(mesh->VY, mesh->Nnodes, MPI_FLOAT,
                  dest, tag_3, MPI_COMM_WORLD, &requestOut_3);
        MPI_Wait(&requestOut_3, &statusOut_3);

        free(messageOut);
        free(messageOut_1);

    }

    //for rank 0
    int NquadsLocal = chunk + (rank<remainder);
    mesh->Nelements = NquadsLocal;

    /* where do these elements start ? */
    int start = rank*chunk + mymin(rank, remainder);
    int end = start + NquadsLocal-1;

    mesh->EToV = (iint*)calloc(NquadsLocal*mesh->Nverts,sizeof(iint));

    int i;
    for(i=0;i<NquadsLocal;i++){
            cnt = start + i;
            mesh->EToV[i*mesh->Nverts+0] = tmpEToV[cnt*mesh->Nverts+0];
            mesh->EToV[i*mesh->Nverts+1] = tmpEToV[cnt*mesh->Nverts+1];
            mesh->EToV[i*mesh->Nverts+2] = tmpEToV[cnt*mesh->Nverts+2];
            mesh->EToV[i*mesh->Nverts+3] = tmpEToV[cnt*mesh->Nverts+3];
    }
    
    free(tmpEToV);

  }

  if(rank>0){  //other rank receive message
    int src = 0;
    MPI_Request requestIn, requestIn_1, requestIn_2, requestIn_3;
    MPI_Status statusIn, statusIn_1, statusIn_2, statusIn_3;
    int tag = 9, tag_1 = 99, tag_2 = 999, tag_3 = 9999;

    //mesh->Nnodes and mesh->Nelements receive Nnodes and NquadsLocal
    iint *messageIn = (iint*) calloc(2, sizeof(iint));

    MPI_Irecv(messageIn, 2, MPI_INT, src, tag, MPI_COMM_WORLD, &requestIn);
    MPI_Wait(&requestIn, &statusIn);

    mesh->Nnodes = messageIn[0];
    mesh->Nelements = messageIn[1];

    //receive mesh->EToV
    mesh->EToV
        = (iint*) calloc(mesh->Nelements*mesh->Nverts,
                         sizeof(iint));
  
    MPI_Irecv(mesh->EToV, mesh->Nelements*mesh->Nverts, MPI_INT, 
              src, tag_1, MPI_COMM_WORLD, &requestIn_1);
    MPI_Wait(&requestIn_1, &statusIn_1);

    //receive VX VY
    mesh->VX = (dfloat*) calloc(mesh->Nnodes, sizeof(dfloat));
    mesh->VY = (dfloat*) calloc(mesh->Nnodes, sizeof(dfloat));
    MPI_Irecv(mesh->VX, mesh->Nnodes, MPI_FLOAT,
              src, tag_2, MPI_COMM_WORLD, &requestIn_2);
    MPI_Wait(&requestIn_2, &statusIn_2);
    MPI_Irecv(mesh->VY, mesh->Nnodes, MPI_FLOAT,
              src, tag_3, MPI_COMM_WORLD, &requestIn_3);
    MPI_Wait(&requestIn_3, &statusIn_3);
    
    free(messageIn);
  }  

  //return mesh
  return mesh;

}
  
