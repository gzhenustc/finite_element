#include <stdlib.h>
#include <stdio.h>
#include "mpi.h"
#include "meshQuad2D.h"

#include "parmetis.h"
#include "parmetisdefs.h"

void meshParmetisPartition2D(mesh2D *mesh){
  
  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  int Nverts = mesh->Nverts;
  
  int *allNelements = (int*) calloc(size, sizeof(int));
  
  /* local number of elements */
  int Nelements = mesh->Nelements;
  
  /* find number of elements on all processors */
  MPI_Allgather(&Nelements, 1, MPI_INT, allNelements, 1, MPI_INT, MPI_COMM_WORLD);


  /* element distribution -- cumulative element count on processes */
  idx_t *elmdist = (idx_t*) calloc(size+1, sizeof(idx_t)); // element starts
  
  int e,r,n;
  
  elmdist[0] = 0;
  for(r=0;r<size;++r)
    elmdist[r+1] = elmdist[r] + allNelements[r];
  
  /* list of element starts */
  idx_t *eptr = (idx_t*) calloc(Nelements+1, sizeof(idx_t)); // element starts
  
  eptr[0] = 0;
  for(e=0;e<Nelements;++e)
    eptr[e+1] = eptr[e] + Nverts;
  
  /* local element to vertex */
  idx_t *eind = (idx_t*) calloc(Nverts*Nelements, sizeof(idx_t)); // element starts
  
  for(e=0;e<Nelements;++e)
    for(n=0;n<Nverts;++n)
      eind[e*Nverts+n] = mesh->EToV[e*Nverts+n];
  
  /* weight per element */
  idx_t *elmwgt = (idx_t*) calloc(Nelements, sizeof(idx_t)); // element starts

 for(e=0;e<Nelements;++e)
   elmwgt[e] = 1.;

 /* weight flag */
 int wgtflag = 0;

 /* number flag (1=fortran, 0=c) */
 int numflag = 0;
 
 /* ncon = 1 */
 int ncon = 1;
 
 /* nodes on element face */
 int ncommonnodes = 2;

 /* number of partitions */
 int nparts = size;
 
 /* tpwgts */
 float *tpwgts = (float*) calloc(Nelements, sizeof(float));
 
 for(e=0;e<Nelements;++e)
   tpwgts[e] = 1./(float)size;
 
 #define MAXNCON 32
 float ubvec[MAXNCON];
 
 for (n=0; n<ncon; ++n)
   ubvec[n] = UNBALANCE_FRACTION;
 
 int options[10];
 
 options[0] = 1;
 options[PMV3_OPTION_DBGLVL] = 7;
 
 options[PMV3_OPTION_SEED] = 0;
 
 int edgecut;
 
 idx_t *part = (idx_t*) calloc(Nelements, sizeof(idx_t)); // element starts
 
 MPI_Comm comm;
 MPI_Comm_dup(MPI_COMM_WORLD, &comm);

 ParMETIS_V3_PartMeshKway
   (elmdist,
    eptr,
    eind,
    elmwgt,
    &wgtflag,
    &numflag,
    &ncon,
    &ncommonnodes,
    &nparts,
    tpwgts,
    ubvec,
    options,
    &edgecut,
    part,
    &comm);

 /* now repartition EToV */

 /* add up how many ints need to be sent to each process (element count in each partition) */
 int *outNdata = (int*) calloc(size, sizeof(int));
 for(e=0;e<Nelements;++e)
   outNdata[part[e]] += Nverts;
 
 /* get count of incoming elements from each process */
 int *inNdata = (int*) calloc(size, sizeof(int));
 MPI_Alltoall(outNdata, 1, MPI_INT,
	      inNdata,  1, MPI_INT,
	      MPI_COMM_WORLD);

 /* find offsets into outgoing array for each rank data */
 int *outStarts = (int*) calloc(size, sizeof(int));
 for(r=1;r<size;++r)
   outStarts[r] = outStarts[r-1]+outNdata[r-1];

 /* find offsets into incoming array for each rank's data */
 int *inStarts = (int*) calloc(size, sizeof(int));
 for(r=1;r<size;++r)
   inStarts[r] = inStarts[r-1]+inNdata[r-1];
 
 /* create array for outgoing data */
 int *outEToV = (int*) calloc(Nelements*Nverts, sizeof(int));
 int *outCnt  = (int*) calloc(size, sizeof(int));
 for(r=0;r<size;++r)
   outCnt[r] = outStarts[r];
 
 for(e=0;e<Nelements;++e){
   for(n=0;n<Nverts;++n){
     outEToV[outCnt[part[e]]] = mesh->EToV[e*Nverts+n];
     ++outCnt[part[e]];
   }
 }
 
 // reset number of elements
 Nelements = 0;
 for(r=0;r<size;++r){
   //   printf("rank %d gets %d new elements from rank %d \n", rank, inNdata[r]/Nverts, r);
   Nelements += inNdata[r]/Nverts;
 }

 /* send elements to their new rank */
 iint *inEToV = (iint*) calloc(Nelements*Nverts, sizeof(iint));
 
 MPI_Alltoallv(outEToV, outNdata, outStarts, MPI_IINT,
	       inEToV,   inNdata,  inStarts, MPI_IINT, 
	       MPI_COMM_WORLD);
 
 free(mesh->EToV);

 // scrape EToV from inEToV (may be different type iint to EToV)
 mesh->EToV = (iint*) calloc(Nelements*Nverts, sizeof(iint));
 for(e=0;e<Nelements;++e)
   for(n=0;n<Nverts;++n)
     mesh->EToV[e*Nverts+n] = inEToV[e*Nverts+n];
 
 // reset element count
 mesh->Nelements = Nelements;
 
 // free temporaries
 free(allNelements);
 free(tpwgts);
 free(outNdata);
 free(outStarts);
 free(outEToV);
 free(outCnt);
 free(inNdata);
 free(inStarts);
 free(inEToV);

 free(elmdist);
 free(eptr);
 free(eind);
 free(elmwgt);
 free(part);

}
