#include "meshQuad2D.h"
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define mor_order "Morton"
#define hil_order "Hilbert"
#define par_order "Parmetis"

int main(int argc, char **argv){

  /* start MPI */
  MPI_Init(&argc, &argv);
 
  int rank, size;

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);


  mesh2D *mesh;

  mesh = meshParallelReaderQuad2D(argv[1]);

  char order[BUFSIZ];
  
  int r;
  if(rank == 0){

     printf("Please specify partition method, Morton, Hilbert or Parmetis:\n");
     scanf("%s", order);

     for(r=1;r<size;++r){
         MPI_Send(order, BUFSIZ, MPI_CHAR, r, 99, MPI_COMM_WORLD);
     }
   }

  if(rank >0){
      MPI_Status status;
      MPI_Recv(order, BUFSIZ, MPI_CHAR, 0, 99, MPI_COMM_WORLD, &status);
   }

  if(strcmp(order, par_order) == 0)
        meshParmetisPartition2D(mesh);
  else
        meshGeometricPartitionQuad2D(mesh, order);


  meshParallelConnectQuad2D(mesh);
  
  //meshParallelPrintQuad2D(mesh);

  meshPartitionAnalysisQuad2D(mesh);

  //create vtu file name
  char vtuName[BUFSIZ];
  sprintf(vtuName, "%s.vtu", strtok(argv[1], "."));
  meshVTU2D(mesh, vtuName);

  /* finalize MPI */
  MPI_Finalize();

  exit(0);  
  return 0;
}
