#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "mpi.h"

#include "meshQuad2D.h"

void meshVTU2D(mesh2D *mesh, char *fileName){
  
  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  FILE *fp;
  iint *allNelements = (iint*) calloc(size, sizeof(iint));
  iint *tmpEToV = NULL;
  iint totalNelements = 0, maxNelements = 0;

  if(rank==0){
    fp = fopen(fileName, "w");
    printf("fp=%p\n for fileName=%s\n",fp, fileName);
  }

  // gather element counts to root
  MPI_Allgather(&(mesh->Nelements), 1, MPI_IINT, 
		allNelements, 1, MPI_IINT, 
		MPI_COMM_WORLD);
 
  
  if(rank==0){
    int r;
    for(r=0;r<size;++r){
      totalNelements+=allNelements[r];
      maxNelements = mymax(maxNelements, allNelements[r]);
    }
    
    fprintf(fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
    fprintf(fp, "  <UnstructuredGrid>\n");

    fprintf(fp, "    <Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\">\n", mesh->Nnodes, totalNelements);

    // write out nodes
    fprintf(fp, "      <Points>\n");
    fprintf(fp, "        <DataArray type=\"Float32\" NumberOfComponents=\"3\" Format=\"ascii\">\n");

    int n;
    for(n=0;n<mesh->Nnodes;++n){
      fprintf(fp, "          %f %f 0.0\n", mesh->VX[n], mesh->VY[n]);
    }

    fprintf(fp, "        </DataArray>\n");
    fprintf(fp, "      </Points>\n");


    // write out rank
    fprintf(fp, "      <CellData Scalars=\"scalars\">\n");
    fprintf(fp, "        <DataArray type=\"Int32\" Name=\"element_rank\" Format=\"ascii\">\n");

    int e;
    for(r=0;r<size;++r){
      for(e=0;e<allNelements[r];++e)
	fprintf(fp, "         %d\n", r);
    }

    fprintf(fp, "       </DataArray>\n");
    fprintf(fp, "     </CellData>\n");
    tmpEToV = (iint*) calloc(maxNelements*mesh->Nverts, sizeof(iint));
    
    fprintf(fp, "    <Cells>\n");
    fprintf(fp, "      <DataArray type=\"Int32\" Name=\"connectivity\" Format=\"ascii\">\n");
  }

  if(rank==0){
    int e, n;
    for(e=0;e<mesh->Nelements;++e){
      fprintf(fp, "        ");
      for(n=0;n<mesh->Nverts;++n)
	fprintf(fp, "%d ", mesh->EToV[e*mesh->Nverts+n]);
      fprintf(fp, "\n");
    }
  }

  int r;
  for(r=1;r<size;++r){

    if(rank==r)
      MPI_Send(mesh->EToV, mesh->Nelements*mesh->Nverts, MPI_IINT, 0, 666, MPI_COMM_WORLD);

    if(rank==0){
      //tmpEToV = (iint*) calloc(maxNelements*mesh->Nverts, sizeof(iint));
      MPI_Status status;
      MPI_Recv(tmpEToV, allNelements[r]*mesh->Nverts, MPI_IINT, r, 666, MPI_COMM_WORLD, &status);
    }

    if(rank==0){
      int e, n;
      for(e=0;e<allNelements[r];++e){
	fprintf(fp, "        ");
	for(n=0;n<mesh->Nverts;++n)
	  fprintf(fp, "%d ", tmpEToV[e*mesh->Nverts+n]);
	fprintf(fp, "\n");
      }
    }
  }

  
  if(rank==0){
    fprintf(fp, "        </DataArray>\n");
    
    fprintf(fp, "        <DataArray type=\"Int32\" Name=\"offsets\" Format=\"ascii\">\n");
    int e;
    for(e=0;e<totalNelements;++e){
      if(e%10==0) fprintf(fp, "        ");
      fprintf(fp, "%d ", (e+1)*mesh->Nverts);
      if(((e+1)%10==0) || (e==totalNelements-1))
	fprintf(fp, "\n");
    }
    fprintf(fp, "       </DataArray>\n");
    
    fprintf(fp, "       <DataArray type=\"Int32\" Name=\"types\" Format=\"ascii\">\n");
    for(e=0;e<totalNelements;++e){
      if(e%10==0) fprintf(fp, "        ");
      fprintf(fp, "5 ");
      if(((e+1)%10==0) || e==(totalNelements-1))
	fprintf(fp, "\n");
    }
    fprintf(fp, "        </DataArray>\n");
    fprintf(fp, "      </Cells>\n");
    fprintf(fp, "    </Piece>\n");
    fprintf(fp, "  </UnstructuredGrid>\n");
    fprintf(fp, "</VTKFile>\n");
    fclose(fp);
  }

  MPI_Barrier(MPI_COMM_WORLD);
  free(tmpEToV);
  free(allNelements);

}
