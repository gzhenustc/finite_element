#define dfloat float
#define iint int
#define MPI_IINT MPI_INT
#define MPI_DFLOAT MPI_FLOAT
#define iintFormat "%d"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    iint Nnodes;
    dfloat *VX;
    dfloat *VY;
    iint Nelements;
    iint *EToV;
    iint *EToE;
    iint *EToF;
    iint *EToP;
    iint Nverts, Nfaces;    
}mesh2D;

mesh2D* meshReaderQuad2D(char *fileName);
mesh2D* meshParallelReaderQuad2D(char *fileName);

void meshPrintQuad2D(mesh2D *mesh);
void meshParallelPrintQuad2D(mesh2D *mesh);

void meshGeometricPartitionQuad2D(mesh2D *mesh, char *order);

void meshParmetisPartition2D(mesh2D *mesh);

void meshConnectQuad2D(mesh2D *mesh);
void meshParallelConnectQuad2D(mesh2D *mesh);

void meshVTU2D(mesh2D *mesh, char *fileName);

void meshPrintFile(mesh2D *mesh, char *fileName);

#define mymax(a,b) ((a>b)?a:b)
#define mymin(a,b) ((a<b)?a:b)

void meshPartitionAnalysisQuad2D(mesh2D *mesh);

void parallelSort(iint N, void *vv, size_t sz,
                  int (*compare)(const void *, const void *),
                  void (*match)(void *, void *)
                  );
