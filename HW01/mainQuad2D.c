#include "meshQuad2D.h"

int main(int argc, char **argv){

  mesh2D *mesh;

  mesh = meshReaderQuad2D(argv[1]);
  
  meshConnectQuad2D(mesh);
  
  meshPrintQuad2D(mesh);

  return 0;
}
