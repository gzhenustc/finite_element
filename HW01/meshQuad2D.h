#define dfloat float
#define iint int

typedef struct {
    iint Nnodes;
    dfloat *VX;
    dfloat *VY;
    iint Nelements;
    iint *EToV;
    iint *EToE;
    iint *EToF;
    iint Nverts, Nfaces;    
}mesh2D;

mesh2D* meshReaderQuad2D(char *fileName);
void meshPrintQuad2D(mesh2D *mesh);
void meshConnectQuad2D(mesh2D *mesh);

#define mymax(a,b) ((a>b)?a:b)
#define mymin(a,b) ((a<b)?a:b)

