#include <stdio.h>
#include <stdlib.h>
#include "meshQuad2D.h"

void meshPrint(mesh2D *mesh){

     int e, n, k, cnt=0;

#if 0
     for(e=0;e<mesh->Nelements;++e){
        for(n=0;n<mesh->Np;++n){
           for(k=0;k<5;++k)
               printf("%f ",mesh->vgeo[cnt]);
               ++cnt;
           printf("\n");
        }
     }
#endif
//#if 0
     for(n=0;n<mesh->Np;++n){
        for(e=0;e<mesh->Nelements;++e){ 
            k = n*mesh->Nelements + e;
            printf("%f ", mesh->x[k]);
        }
        printf("\n");
     }

//#endif

}
