#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "mpi.h"

#include "meshQuad2D.h"

void meshPrintFile(mesh2D *mesh, char *fileName){
  
  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  FILE *fp;
  iint *allNelements = NULL;
  iint *tmpEToV = NULL;
  iint totalNelements = 0, maxNelements = 0;

  if(rank==0){
    fp = fopen(fileName, "w");
  }
  
  allNelements = (iint*) calloc(size, sizeof(iint));

  // gather element counts to root
  MPI_Allgather(&(mesh->Nelements), 1, MPI_IINT, 
		allNelements, 1, MPI_IINT, 
		MPI_COMM_WORLD);

  iint *elementStarts = (iint*) calloc(size, sizeof(iint));
  iint r;
  for(r=1;r<size;++r){
       elementStarts[r] = elementStarts[r-1]+allNelements[r-1];
  }

  
  if(rank==0){

    for(r=0;r<size;++r){
      totalNelements+=allNelements[r];
      maxNelements = mymax(maxNelements, allNelements[r]);
    }

    fprintf(fp, "NumberOfCells %d\n", totalNelements);
    
    // write out nodes
    fprintf(fp, "Points\n");
    int n;
    for(n=0;n<mesh->Nnodes;++n){
      fprintf(fp, "%d %f %f 0.0\n", n, mesh->VX[n], mesh->VY[n]);
    }

    
    //tmpEToV = (iint*) calloc(maxNelements*mesh->Nverts, sizeof(iint));
    
    fprintf(fp, "rank %d\n", rank);

  }


  if(rank==0){
    int e, n;
    for(e=0;e<mesh->Nelements;++e){
      fprintf(fp,"%d ",e+elementStarts[rank]);
      for(n=0;n<mesh->Nverts;++n)
	fprintf(fp, "%d ", mesh->EToV[e*mesh->Nverts+n]);
      fprintf(fp, "\n");
    }
  }
  
  int e, n;
  for(r=1;r<size;++r){

    if(rank==r)
      MPI_Send(mesh->EToV, mesh->Nelements*mesh->Nverts, MPI_IINT, 0, 666, MPI_COMM_WORLD);

    if(rank==0){
      tmpEToV = (iint*) calloc(allNelements[r]*mesh->Nverts, sizeof(iint));
      MPI_Status status;
      MPI_Recv(tmpEToV, allNelements[r]*mesh->Nverts, MPI_IINT, r, 666, MPI_COMM_WORLD, &status);
    }

    if(rank==0){
      fprintf(fp, "rank %d\n", r);
      for(e=0;e<allNelements[r];++e){
	fprintf(fp, "%d ", e+elementStarts[r]);
	for(n=0;n<mesh->Nverts;++n)
	  fprintf(fp, "%d ", tmpEToV[e*mesh->Nverts+n]);
	fprintf(fp, "\n");
      }
    }
  }

  if(rank==0) fclose(fp);
 

  MPI_Barrier(MPI_COMM_WORLD);
  
  free(allNelements);
  free(tmpEToV);
  free(elementStarts);
}
