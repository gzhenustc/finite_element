#include "meshQuad2D.h"


void meshPrintQuad2D(mesh2D *mesh){

    int e;  
    //print out vertices
    printf("Vertices:\n");
    for(e=0;e<mesh->Nelements;++e){
       printf("%d %d %d %d\n",
              mesh->EToV[e*mesh->Nverts+0],
              mesh->EToV[e*mesh->Nverts+1],
              mesh->EToV[e*mesh->Nverts+2],
              mesh->EToV[e*mesh->Nverts+3]);
    }
    
    //print out element to element and element to face connectivity
    printf("\nEToE:\n");
    for(e=0;e<mesh->Nelements;++e){
       printf("%d %d %d %d\n",
              mesh->EToE[e*mesh->Nfaces+0],
              mesh->EToE[e*mesh->Nfaces+1],
              mesh->EToE[e*mesh->Nfaces+2],
              mesh->EToE[e*mesh->Nfaces+3]);
    }
    
    printf("\nEToF:\n");
    for(e=0;e<mesh->Nelements;++e){
       printf("%d %d %d %d\n",
              mesh->EToF[e*mesh->Nfaces+0],
              mesh->EToF[e*mesh->Nfaces+1],
              mesh->EToF[e*mesh->Nfaces+2],
              mesh->EToF[e*mesh->Nfaces+3]);
    }

}
