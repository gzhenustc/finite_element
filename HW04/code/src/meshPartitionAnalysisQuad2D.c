#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include "meshQuad2D.h"


void meshPartitionAnalysisQuad2D(mesh2D *mesh){

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);


    iint *otherNelements = (iint*) calloc(size, sizeof(iint));
    MPI_Allgather(&(mesh->Nelements), 1, MPI_IINT,
                  otherNelements, 1, MPI_IINT,
                  MPI_COMM_WORLD);

    // allocate space
    iint *elementStarts = (iint*) calloc(size, sizeof(iint));
    
    int cnt;

    int connect[size];
    for(cnt=0;cnt<size;cnt++)
         connect[cnt]=0;

    int *tmpcnt, totalNelements = 0;

    iint r;
    for(r=1;r<size;++r){
       elementStarts[r] = elementStarts[r-1]+otherNelements[r-1];
    }

    int r1;
    iint e1, f1;
   
    //print Nelements
    if(rank==0){

        for(r=0;r<size;++r)
            totalNelements+=otherNelements[r];
        
        int chunk = totalNelements/size;
        int remainder = totalNelements - chunk*size;

        for(r=0;r<size;++r){
            int originN = chunk + (r<remainder);

            printf("rank %d newElements = %d (originally %d)\n", r, otherNelements[r], originN);
        }

        r1 = 0;
          
        tmpcnt = (int*) calloc (size, sizeof(int));

        for(e1=0;e1<mesh->Nelements;++e1){
            iint id = e1*mesh->Nfaces;
            for(f1=0;f1<mesh->Nfaces;++f1){
                iint e2 = mesh->EToE[id+f1];
                iint f2 = mesh->EToF[id+f1];
                iint r2 = mesh->EToP[id+f1];

                for(r=0;r<size;r++){
                    if(r2 == r){
                        connect[r]++;
                        break;
                    }
                }

            }
          }
           
          int Nconnections = 0, Ncomms = 0;
          for(r=0;r<size;r++){
             if(connect[r]>0) Nconnections++;
             Ncomms+=connect[r];
          }

          printf("r: %02d [ ",rank);
          for(e1=0;e1<size;e1++)
              printf("%04d ",connect[e1]);
          printf("] (Nconnections=%d, Ncomms=%d)\n", Nconnections, Ncomms);


    }
    
    for(r1=1;r1<size;++r1){

      if(rank==r1){

         for(e1=0;e1<mesh->Nelements;++e1){
            iint id = e1*mesh->Nfaces;
            for(f1=0;f1<mesh->Nfaces;++f1){
                iint e2 = mesh->EToE[id+f1];
                iint f2 = mesh->EToF[id+f1];
                iint r2 = mesh->EToP[id+f1];

                for(r=0;r<size;r++){
                    if(r2 == r){
                        connect[r]++;
                        break;
                    }
                }

            }
          } 

          //send   
          MPI_Send(connect, size, MPI_INT, 0, 99, MPI_COMM_WORLD);
     
      }

      if(rank==0){

          MPI_Status status;
          MPI_Recv(tmpcnt, size, MPI_INT, r1, 99, MPI_COMM_WORLD, &status);
         
          int Nconnections = 0, Ncomms = 0;
          for(r=0;r<size;r++){
             if(tmpcnt[r]>0) Nconnections++;
             Ncomms+=tmpcnt[r];
          }


          printf("r: %02d [ ",r1);
          for(e1=0;e1<size;e1++)
              printf("%04d ", tmpcnt[e1]);
          printf("] (Nconnections=%d, Ncomms=%d)\n", Nconnections, Ncomms);  
          
      }

    }


    MPI_Barrier(MPI_COMM_WORLD);
    free(otherNelements);
    free(elementStarts);
    free(tmpcnt);
}
