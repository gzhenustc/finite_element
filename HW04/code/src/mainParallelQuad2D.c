#include "meshQuad2D.h"
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define mor_order "Morton"
#define hil_order "Hilbert"

int main(int argc, char **argv){

  /* start MPI */
  MPI_Init(&argc, &argv);
 
  int rank, size;

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);


  mesh2D *mesh;

  mesh = meshParallelReaderQuad2D(argv[1]);

  //int specify polynomial degree
  int N = atoi(argv[2]);

  char order[BUFSIZ] = "Morton";
  

  meshGeometricPartitionQuad2D(mesh, order);
  

  //connect elements using parallel sort
  meshParallelConnectQuad2D(mesh);

  //print out connectivity statistics
  meshPartitionAnalysisQuad2D(mesh);
  
  //load reference (r,s) element nodes
  meshLoadReferenceNodesQuad2D(mesh, N);


  //compute physical (x,y) locations of the element nodes
  meshPhysicalNodesQuad2D(mesh);

  
  //compute geometric factors
  meshGeometricFactorsQuad2D(mesh);


  // compute samples of q at interpolation nodes
  dfloat *q = (dfloat*) calloc(mesh->Nelements*mesh->Np,
                               sizeof(dfloat));
  dfloat *dqdx = (dfloat*) calloc(mesh->Nelements*mesh->Np,
                                  sizeof(dfloat));
  dfloat *dqdy = (dfloat*) calloc(mesh->Nelements*mesh->Np,
                                  sizeof(dfloat));   

  iint cnt = 0, n, e;
  for(e=0;e<mesh->Nelements;++e){
    for(n=0;n<mesh->Np;++n){
      q[cnt] = mesh->x[cnt];
      ++cnt;
    }
  }

  //physical x,y derivatives of q data 
  // interpolated at interpolation nodes
  meshGradientQuad2D(mesh, q, dqdx, dqdy);

  dfloat maxError = 0;
  cnt = 0;
  for(e=0;e<mesh->Nelements;++e){
    for(n=0;n<mesh->Np;++n){
      maxError = mymax(maxError, fabs(dqdx[cnt])-1);
      ++cnt;
    }
  }

  dfloat globalError;
  MPI_Allreduce(&maxError,&globalError, 1, MPI_DFLOAT,
                MPI_MAX, MPI_COMM_WORLD);

  if(rank==0){
       printf("globalError = %6.5E\n", globalError);
       printf("rank %d localError = %6.5E\n", rank, maxError);       
  }
  else 
       printf("rank %d localError = %6.5E\n", rank, maxError);
  

  //create vtu file name
  char vtuName[BUFSIZ];
  sprintf(vtuName, "%s.vtu", strtok(argv[1], "."));
  meshVTU2D(mesh, vtuName);

  /* finalize MPI */
  MPI_Finalize();

  exit(0);  
  return 0;
}
