
#include "occa.hpp"
#include "mesh2D.h"

void occaOptimizeGradient2D(mesh2D *mesh, dfloat *q, dfloat *dqdx, dfloat *dqdy){

  // print all devices 
  occa::printAvailableDevices();

  // build OCCA device
  occa::device device;

  //---[ Device setup with string flags ]-------------------
    device.setup("mode = CUDA, deviceID = 0");
  //  device.setup("mode = OpenCL  , platformID = 0, deviceID = 1");

  // set up compiler flags
  occa::kernelInfo info;
  info.addDefine("p_Np", mesh->Np);
  info.addDefine("p_Nvgeo", mesh->Nvgeo);

#if 0
  info.addCompilerFlag("--ftz=true");
  info.addCompilerFlag("--prec-div=false");
  info.addCompilerFlag("--prec-sqrt=false");
  info.addCompilerFlag("--use_fast_math");
  info.addCompilerFlag("--fmad=true"); // compiler option for cuda
#endif

  /* build set of kernels to test */
  int Nkernels = 8;
  occa::kernel meshGradient2DKernels[Nkernels];
  char kernelNames[Nkernels][BUFSIZ];
  for(int ker=0;ker<Nkernels;++ker){
    sprintf(kernelNames[ker], "meshGradient2D_v%d", ker);
    
    meshGradient2DKernels[ker] =
      device.buildKernelFromSource("src/meshOptimizedGradient2D.okl", kernelNames[ker], info);
  }
  
  // allocate DEVICE arrays
  occa::memory o_q  = 
    device.malloc(mesh->Np*mesh->Nelements*sizeof(float), q);
  occa::memory o_dqdx  = 
    device.malloc(mesh->Np*mesh->Nelements*sizeof(float), dqdx);
  occa::memory o_dqdy  = 
    device.malloc(mesh->Np*mesh->Nelements*sizeof(float), dqdy);
  occa::memory o_vgeo  = 
    device.malloc(mesh->Nvgeo*mesh->Nelements*sizeof(float),
		  mesh->vgeo);

  // create transposes of Dr and Ds
  dfloat *DrT = (dfloat*) calloc(mesh->Np*mesh->Np, sizeof(dfloat));
  dfloat *DsT = (dfloat*) calloc(mesh->Np*mesh->Np, sizeof(dfloat));
  for(int n=0;n<mesh->Np;++n){
    for(int m=0;m<mesh->Np;++m){
      DrT[n+m*mesh->Np] = mesh->Dr[n*mesh->Np+m];
      DsT[n+m*mesh->Np] = mesh->Ds[n*mesh->Np+m];
    }
  }
      
  // allocate operator matrices
  occa::memory o_Dr  = device.malloc(mesh->Np*mesh->Np*sizeof(float), mesh->Dr);
  occa::memory o_Ds  = device.malloc(mesh->Np*mesh->Np*sizeof(float), mesh->Ds);
  occa::memory o_DrT = device.malloc(mesh->Np*mesh->Np*sizeof(float), DrT);
  occa::memory o_DsT = device.malloc(mesh->Np*mesh->Np*sizeof(float), DsT);
  
  // enable timing
  occa::initTimer(device);

  // run each kernel 5 times
  for(int ker=0;ker<Nkernels;++ker){
    device.finish();
    occa::tic(kernelNames[ker]);
    int Ntests = 5;
    for(int test=0;test<Ntests;++test){
      if(test<4)
	meshGradient2DKernels[ker](mesh->Nelements, mesh->Np, mesh->Nvgeo, o_vgeo, o_Dr, o_Ds, o_q, o_dqdx, o_dqdy);
      else
	meshGradient2DKernels[ker](mesh->Nelements, mesh->Np, mesh->Nvgeo, o_vgeo, o_DrT, o_DsT, o_q, o_dqdx, o_dqdy);
    }
    device.finish();
    occa::toc(kernelNames[ker]);
  }
  
  // copy from DEVICE to HOST array
  o_dqdx.copyTo(dqdx);
  o_dqdy.copyTo(dqdy);

  // print timings
  occa::printTimer();
  

  
}



