
#include <stdio.h>
#include <stdlib.h>
#include "meshQuad2D.h"

void meshGeometricFactorsQuad2D(mesh2D *mesh){

  /* unified storage array for geometric factors */
  mesh->Nvgeo = 5;
  mesh->vgeo = (dfloat*) calloc(mesh->Nelements*mesh->Nvgeo*mesh->Np, 
				sizeof(dfloat));

  iint e, cnt, k;  
  for(e=0;e<mesh->Nelements;++e){ /* for each element */

    /* find vertex indices and physical coordinates */
    int id = e*mesh->Nverts+0;

    dfloat xe1 = mesh->EX[id+0];
    dfloat xe2 = mesh->EX[id+1];
    dfloat xe3 = mesh->EX[id+2];
    dfloat xe4 = mesh->EX[id+3];

    dfloat ye1 = mesh->EY[id+0];
    dfloat ye2 = mesh->EY[id+1];
    dfloat ye3 = mesh->EY[id+2];
    dfloat ye4 = mesh->EY[id+3];

   // printf("xe1 ye1 xe2 ye2: %f %f %f %f\n",xe1,ye1,xe2,ye2);

    /* compute geometric factors for affine coordinate transform*/
    iint n;
    for(n=0;n<mesh->Np;++n){ /* for each node */
      /* (r,s) coordinates of interpolation nodes*/
      dfloat rn = mesh->r[n];
      dfloat sn = mesh->s[n];

      dfloat xr = 0.25*(1.0-sn)*(xe2-xe1) + 0.25*(1.0+sn)*(xe3-xe4);
      dfloat xs = 0.25*(1.0-rn)*(xe4-xe1) + 0.25*(1.0+rn)*(xe3-xe2);
      dfloat yr = 0.25*(1.0-sn)*(ye2-ye1) + 0.25*(1.0+sn)*(ye3-ye4);
      dfloat ys = 0.25*(1.0-rn)*(ye4-ye1) + 0.25*(1.0+rn)*(ye3-ye2);

      dfloat J = xr*ys - yr*xs;
      dfloat rx =  (1.0/J)*ys;
      dfloat ry = -(1.0/J)*xs;
      dfloat sx = -(1.0/J)*yr;
      dfloat sy =  (1.0/J)*xr;

      cnt = e*mesh->Np + n*mesh->Nvgeo;
    
      /* store geometric factors */
      mesh->vgeo[cnt + RXID] = rx;
      mesh->vgeo[cnt + RYID] = ry;
      mesh->vgeo[cnt + SXID] = sx;
      mesh->vgeo[cnt + SYID] = sy;
      mesh->vgeo[cnt +  JID] = J;


    }
  }
}
