#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv){

   int rank, size;

   /* start up MPI */
   MPI_Init(&argc, &argv);
   
   /* find MPI rank 0-indexed process number */
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);   

   /* find number of MPI process - called "size" of the MPI job */
   MPI_Comm_size(MPI_COMM_WORLD, &size);
   
   /* print message from each process */
   printf("process rank %d of %d says hello world\n", rank, size);

   /* send a message from process 0 to process 1 */
   int N=10, n;

   if(rank == 0){ //send a message
     //outgoing message
     int *messageOut = (int*) calloc(N, sizeof(int));
     for(n=0;n<N;++n) messageOut[n] = n;

     // request a message to be sent
     MPI_Request requestOut;
     int dest = 1; //destination rank
     int tag = 999;
     MPI_Isend(messageOut, N, MPI_INT, dest, tag, MPI_COMM_WORLD, &requestOut);

     // rank 0 can do stuff before waiting
     // (as long as it doesn't mess with the message buffer)
     MPI_Status statusOut;
     MPI_Wait(&requestOut, &statusOut);
   
   }

   if(rank == 1){ //receive a message
     //incoming message
     int *messageIn = (int*) calloc(N, sizeof(int));

     // request a message to be received
     MPI_Request requestIn;
     int src = 0; //source rank
     int tag = 999;
     MPI_Irecv(messageIn, N, MPI_INT, src, tag, MPI_COMM_WORLD, &requestIn);

     // rank 1 can do stuff before waiting
     // (as long as it doesn't mess with the message buffer)
     MPI_Status statusIn;
     MPI_Wait(&requestIn, &statusIn);
     
     for(n=0;n<N;++n)
        printf("messageIn[%d]=%d\n", n, messageIn[n]);
   }


   /* sometimes useful to make sure all processes get to a point in the code */
   MPI_Barrier(MPI_COMM_WORLD);

   /* often useful to find a max (or min or sum) of varibles distributed amongst    processes */
   int *numbers = (int*) calloc(N, sizeof(int));
   int *maxNumbers = (int*) calloc(N, sizeof(int));
   for(n=0;n<N;++n)
       numbers[n] = rank;
   MPI_Allreduce(numbers, maxNumbers, N, MPI_INT, MPI_MAX, MPI_COMM_WORLD);

   printf("maxNumbers[0] = %d \n", maxNumbers[0]);

   /* finalize */
   MPI_Finalize();

   exit(0);
   return 0;

}
