#ifndef MESH2D_H
#define MESH2D_H 2

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if 1
#define dfloat float
#define iint int
#define MPI_IINT MPI_INT
#define MPI_DFLOAT MPI_FLOAT
#define iintFormat "%d"
#define dfloatFormat "%f"
#else
#define iint int
#define dfloat double
#define MPI_IINT MPI_INT
#define MPI_DFLOAT MPI_DOUBLE
#define iintFormat "%d"
#define dfloatFormat "%lf"
#endif


typedef struct {
    iint Nnodes;
    dfloat *EX;
    dfloat *EY;

    iint Nelements;
    iint *EToV;
    iint *EToE;
    iint *EToF;
    iint *EToP;
    iint Nverts, Nfaces;    
    
    // volumeGeometricFactors
    dfloat *vgeo;
    iint Nvgeo;

    // node info
    iint N, Np;
    dfloat *r, *s; // coordinates of local nodes
    dfloat *Dr, *Ds; // collocation differentiation matrices
    dfloat *x, *y; // coordinates of physical nodes


}mesh2D;

mesh2D* meshReaderQuad2D(char *fileName);
mesh2D* meshParallelReaderQuad2D(char *fileName);

void meshGeometricPartitionQuad2D(mesh2D *mesh, char *order);

void meshParmetisPartition2D(mesh2D *mesh);

void meshConnectQuad2D(mesh2D *mesh);
void meshParallelConnectQuad2D(mesh2D *mesh);

void meshPrintQuad2D(mesh2D *mesh);
void meshParallelPrintQuad2D(mesh2D *mesh);

void meshVTU2D(mesh2D *mesh, char *fileName);

void meshPrintFile(mesh2D *mesh, char *fileName);

#define mymax(a,b) ((a>b)?a:b)
#define mymin(a,b) ((a<b)?a:b)

void parallelSort(iint N, void *vv, size_t sz,
                  int (*compare)(const void *, const void *),
                  void (*match)(void *, void *)
                  );

#define RXID 0
#define RYID 1
#define SXID 2
#define SYID 3
#define  JID 4

void meshGeometricFactorsQuad2D(mesh2D *mesh);

void meshPhysicalNodesQuad2D(mesh2D *mesh);

void meshLoadReferenceNodesQuad2D(mesh2D *mesh, int N);

void meshPrint(mesh2D *mesh);

void meshGradientQuad2D(mesh2D *mesh,
                    dfloat *q,
                    dfloat *dqdx,
                    dfloat *dqdy
                    );

//print out parallel partition i
void meshPartitionAnalysisQuad2D(mesh2D *mesh);

void occaTest(mesh2D *mesh);

#endif
