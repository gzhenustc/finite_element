
function [r,s] = Node2D(N);

[ro] = JacobiGL(0,0,N);

%%number of LGL points
Ngl = length(ro);

%%define tensor product
r = zeros(Ngl*Ngl,1);
s = zeros(Ngl*Ngl,1);

for i=1:Ngl
  for j=1:Ngl
     r((i-1)*Ngl+j,1) = ro(i);
     s((i-1)*Ngl+j,1) = ro(j);
  end
end

return;
